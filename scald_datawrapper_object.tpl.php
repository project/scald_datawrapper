<?php
/**
 * @file
 * Default theme implementation for the Scald Datawrapper chart
 */
?>
<iframe width="<?php print $vars['chart_width'] ?>" height="<?php print $vars['chart_height'] ?>" src="<?php print $vars['chart_url'] ?>" frameborder="0" allowfullscreen></iframe>
