
-- SUMMARY --

* This module provides Datawrapper chart import inside Scald.
  It creates a scald provider allowing users to add Media Atoms of type
Datawrapper chart.

* This project includes all the features needed to create atoms directly
  from Datawrapper, reuse them, and simply embed them into your drupal
  nodes with drag and drop.

* The module extends scald UI to do the following :
  - Import a single Datawrapper chart from its url or its id Automatically
import thumbnails

* See http://drupal.org/node/1895554 for a list of Scald providers
  as separate projects for other great providers.

-- REQUIREMENTS --

* Scald module

* This project use Datawrapper charts, So you need to create charts from this
site :
  http://datawrapper.de


-- INSTALLATION --

* To test it quickly with drush : drush en -y scald_datawrapper
scald_dnd_library mee


-- HOW TO USE --

* You can use charts id to create atom like : weD23,
or you can use url like : http://charts.datawrapper.de/weD23/index.html

-- CONTACT --

Current maintainers :
* Clément Pineau (poukram) - http://drupal.org/user/1798946
